import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;

/**
 * Created by thomasfurst on 31/03/16.
 */
public class RevRandom {

    private JFrame frame;

    private JPanel panel;

    private JLabel image;
    private JLabel name;

    private JButton nextButton;

    private String key;

    private ActionListener checkListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            nextButton.setText("Další");
            name.setText(key);

            nextButton.addActionListener(nextListener);
            nextButton.removeActionListener(checkListener);
        }
    };

    private ActionListener nextListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            generateNew();
            nextButton.setText("Kontrola");

            nextButton.addActionListener(checkListener);
            nextButton.removeActionListener(nextListener);
        }
    };

    private Map<String, String> plants = new HashMap<>();

    public RevRandom(String database) {
        plants = Utils.loadPlants(database);

        frame = new JFrame("RevSys Random");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(800, 400);
        frame.setLocationRelativeTo(null);

        panel = new JPanel(new GridBagLayout());

        image = new JLabel();
        name = new JLabel();
        name.setHorizontalAlignment(JLabel.CENTER);
        nextButton = new JButton("Kontrola");
        nextButton.addActionListener(checkListener);

        generateNew();
        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    public void generateNew() {
        name.setText("");

        Random rand = new Random();
        List<String> keys = new ArrayList<>(plants.keySet());
        key = keys.get(rand.nextInt(keys.size()));

        BufferedImage img = Utils.getRandomImgFromDir(plants.get(key), plants, key);

        double ratio = (double) Math.min((int) (frame.getHeight() * 0.7), img.getHeight()) / (double) img.getHeight();

        int width = (int) (img.getWidth() * ratio);
        int height = (int) (img.getHeight() * ratio);

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.createGraphics();
        g.drawImage(img, 0, 0, width, height, null);
        g.dispose();

        this.image = new JLabel(new ImageIcon(image));

        rebuildUI();
    }

    public void rebuildUI() {
        panel.removeAll();

        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 10;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        c.insets = new Insets(5, 5, 5, 5);
        panel.add(image, c);

        c.gridy = 1;
        c.weightx = c.weighty = 0.5;
        panel.add(name, c);

        c.gridy = 2;
        c.ipadx = c.ipady = 0;
        c.weightx = c.weighty = 0;
        panel.add(nextButton, c);

        frame.revalidate();
        frame.repaint();
    }

}
