import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by thomasfurst on 30/03/16.
 */
public class MarkdownEncode {

    public static String toHTML(String markdown) {
        BufferedReader br = new BufferedReader(new StringReader(markdown));

        ArrayList<String> lines = new ArrayList<>();
        String line = null;

        try {
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        StringBuilder builder = new StringBuilder();
        builder.append("<div>");

        boolean list = false;
        for (int i = 0; i < lines.size(); i++) {
            line = lines.get(i);

            if (line.length() <= 0 || line.matches("^[\\s]")) {
                continue;
            }

            if (line.charAt(0) == '*' && line.length() >= 3) {
                if (!list) {
                    list = true;
                    builder.append("<ul>");
                }

                builder.append("<li>" + line.substring(2, line.length()) + "</li>");
                if (lines.size() == i + 1) { builder.append("</ul>"); }
                continue;
            }

            if (line.charAt(0) != '*' && list == true) {
                list = false;
                builder.append("</ul>");
            }

            // Get h1
            if (line.matches("^[=]*") && i != 0 && lines.get(i -1).charAt(0) != '*') {
                builder.append("<h1>" + lines.get(i - 1) + "</h1>");
                continue;
            }

            // Get h2
            if (line.matches("^[-]*") && i != 0 && lines.get(i - 1).charAt(0) != '*') {
                builder.append("<h2>" + lines.get(i - 1) + "</h2>");
                continue;
            }

            if (lines.size() > i + 1 && lines.get(i + 1).matches("^[=-]*")) {
                continue;
            }

            builder.append("<p>" + line + "</p>");
        }
        builder.append("</div>");

        return builder.toString();
    }

}
