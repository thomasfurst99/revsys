import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.io.*;

/**
 * Created by thomasfurst on 30/03/16.
 */
public class RevTree extends JTree {

    // Node with additional data
    public static class RevNode extends DefaultMutableTreeNode {

        public boolean isLeaf = false;

        public String path;

        public RevNode(String str) {
            this (str, "");
        }

        public RevNode(String str, String path) {
            super(str);

            this.path = path;
        }


    }

    public interface IRevTreeModel {
        void OnNodeSelected(TreeSelectionEvent e);
    }

    public IRevTreeModel model = null;

    public static RevNode getData(File file) {
        RevNode node = null;
        if (file.isDirectory()) {
            if (new File(file.getPath() + "/info.txt").exists()) {
                try {
                    BufferedReader br = new BufferedReader(new FileReader(file.getPath() + "/info.txt"));
                    node = new RevNode(br.readLine(), file.getPath());
                    br.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (node == null) {
            return null;
        }
        File[] files = new File(file.getPath()).listFiles();

        node.isLeaf = true;
        for (File f : files) {
            if (f.isDirectory()) {
                RevNode tmp = getData(f);
                if (tmp != null) {
                    node.add(getData(f));
                    node.isLeaf = false;
                }
            }
        }

        return node;
    }

    public RevTree(RevNode root) {
        super(root);

        ImageIcon icon = new ImageIcon(getClass().getResource("/res/leaf.png"));
        DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
        renderer.setLeafIcon(icon);

        setCellRenderer(renderer);
        setShowsRootHandles(true);
        setRootVisible(false);

        getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                if (model != null) {
                    model.OnNodeSelected(e);
                }
            }
        });
    }

}
