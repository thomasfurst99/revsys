import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Arrays;

/**
 * Created by thomasfurst on 30/03/16.
 */
public class RevSys implements RevTree.IRevTreeModel {

    private JFrame frame;

    private JPanel panel;

    /// Part for inputting path to the database
    private JPanel filePanel;
    private JButton fileButton;
    private RevTree revTree;

    // Panel for data about plants
    private JPanel dataPanel;
    private JTextPane aboutText;
    private JPanel images;

    private JButton testButton;
    private JButton test2Button;

    private String currentPath = "";
    private String database = "";

    private java.util.List<String> extensions = Arrays.asList(".jpg", ".jpeg", ".png");


    public static void main(String... args) {
        new RevSys().createAndShowGUI();
    }

    private void createAndShowGUI() {
        frame = new JFrame("RevSys");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        panel = new JPanel(new GridBagLayout());
        filePanel = new JPanel(new GridBagLayout());

        fileButton = new JButton("Vybrat databázi");
        fileButton.setSize(fileButton.getPreferredSize());
        fileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Vyberte databázi");
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setAcceptAllFileFilterUsed(false);

                if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
                    System.out.println(fileChooser.getSelectedFile());
                    database = fileChooser.getSelectedFile().getPath();
                    revTree = new RevTree(RevTree.getData(fileChooser.getSelectedFile()));
                    rebuildUI();
                }
            }
        });

        revTree = new RevTree(new RevTree.RevNode("root"));


        dataPanel = new JPanel(new GridBagLayout());

        aboutText = new JTextPane();
        aboutText.setContentType("text/html");
        aboutText.setEditable(false);

        images = new JPanel(new FlowLayout());
        images.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

        testButton = new JButton("Test - vyber správnou rostlinu");
        testButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new RevTesting(database);
            }
        });

        test2Button = new JButton("Test - hádej náhodné rostliny");
        test2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new RevRandom(database);
            }
        });

        frame.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
                rebuildUI();
            }

            @Override
            public void componentMoved(ComponentEvent e) {

            }

            @Override
            public void componentShown(ComponentEvent e) {

            }

            @Override
            public void componentHidden(ComponentEvent e) {

            }
        });

        rebuildUI();
        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    private void rebuildUI() {
        panel.removeAll();
        filePanel.removeAll();
        dataPanel.removeAll();

        revTree.model = this;

        GridBagConstraints c = new GridBagConstraints();

        //////////////////
        // File panel
        //////////////////
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.CENTER;
        c.ipady = (int) (frame.getHeight() * 0.3);
        c.ipadx = (int) (frame.getWidth() * 0.35);
        c.weightx = 1.0;
        c.weighty = 1.0;
        filePanel.add(new JScrollPane(revTree), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy++;
        c.ipady = c.ipadx = 0;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.insets = new Insets(5, 0, 0, 0);
        filePanel.add(fileButton, c);

        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 10;
        c.weighty = 1.0;
        c.ipadx = c.ipady = 0;
        c.insets = new Insets(5, 10, 5, 5);
        panel.add(filePanel, c);

        ////////////////////////////////

        ////////////////////
        // Data panel
        ////////////////////

        reloadData();

        c.gridx = 1;
        c.gridy = 0;
        c.ipadx = (int) (frame.getWidth() * 0.65);
        c.ipady = (int) (frame.getHeight() * 0.4);
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.insets = new Insets(5, 5, 5, 10);
        panel.add(new JScrollPane(aboutText), c);


        c.gridx = 0;
        c.gridy = 0;
        c.ipadx = (int) (frame.getWidth() * 0.95);
        c.ipady = (int) (frame.getHeight() * 0.45);
        c.weightx = 1.0;
        c.weighty = 0.04;
        c.insets = new Insets(5, 5, 0, 5);
        dataPanel.add(new JScrollPane(images, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED), c);

        c.gridx = 0;
        c.gridy++;
        c.ipadx = 0;
        c.ipady = 0;
        c.weightx = c.weighty = 0;
        c.insets = new Insets(2, 0, 0, 0);
        dataPanel.add(testButton, c);

        c.gridy++;
        dataPanel.add(test2Button, c);

        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 2;
        c.ipadx = 0;
        c.ipady = 0;
        c.weightx = 20;
        c.weighty = 1.0;
        c.insets = new Insets(5, 5, 5, 5);
        panel.add(dataPanel, c);

        ////////////////////////

        frame.revalidate();
    }

    private void reloadData() {
        if (currentPath != "") {
            ////////////
            // Images
            ////////////

            images.removeAll();
            File file = new File(currentPath);
            if (file.isDirectory()) {
                for (File imageFile : file.listFiles()) {
                    if (imageFile.isFile()) {
                        String extension = imageFile.getAbsolutePath().substring(imageFile.getAbsolutePath().lastIndexOf('.'));
                        if (extensions.contains(extension)) {
                            try {
                                BufferedImage img = ImageIO.read(imageFile);

                                double ratio = (double) Math.min(images.getHeight(), img.getHeight()) / (double) img.getHeight();

                                int width = (int) (img.getWidth() * ratio);
                                int height = (int) (img.getHeight() * ratio);

                                BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                                Graphics g = image.createGraphics();
                                g.drawImage(img, 0, 0, width, height, null);
                                g.dispose();

                                JLabel label = new JLabel(new ImageIcon(image));
                                images.add(label);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            //////////////

            ////////////
            // Text
            ////////////

            file = new File(currentPath + "/info.txt");
            if (file.exists()) {
                String markdown = "";
                try {
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String line;

                    while ((line = br.readLine()) != null) {
                        markdown += line + "\n";
                    }
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                System.out.println(MarkdownEncode.toHTML(markdown));
                aboutText.setText(MarkdownEncode.toHTML(markdown));
            }
            ///////////////
        }
    }

    @Override
    public void OnNodeSelected(TreeSelectionEvent e) {
        RevTree.RevNode node = (RevTree.RevNode) revTree.getLastSelectedPathComponent();
        System.out.println(node.path);
        this.currentPath = node.path;

        rebuildUI();
    }
}
