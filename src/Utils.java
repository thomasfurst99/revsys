import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

/**
 * Created by thomasfurst on 31/03/16.
 */
public class Utils {

    public static final List<String> extensions = Arrays.asList(".jpg", ".jpeg", ".png");

    public static Map<String, String> loadPlants(String path) {
        Map<String, String> dict = new HashMap<>();
        load(new File(path), dict);
        return dict;
    }

    private static void load(File file, Map<String, String> db) {
        boolean dir = false;

        for (File f : file.listFiles()) {
            if (f.isDirectory()) {
                dir = true;
                load(f, db);
            }
        }

        if (!dir) {
            if (file.isDirectory()) {
                if (new File(file.getPath() + "/info.txt").exists()) {
                    try {
                        BufferedReader br = new BufferedReader(new FileReader(file.getPath() + "/info.txt"));
                        db.put(br.readLine(), file.getPath());
                        System.out.println("Loading " + file.getPath());
                        br.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static BufferedImage getRandomImgFromDir(String path, Map<String, String> db, String key) {
        ArrayList<BufferedImage> images = new ArrayList<>();
        Random rand = new Random();

        File file = new File(db.get(key));
        for (File imageFile : file.listFiles()) {
            if (imageFile.isFile()) {
                String extension = imageFile.getAbsolutePath().substring(imageFile.getAbsolutePath().lastIndexOf('.'));
                if (extensions.contains(extension)) {
                    try {
                        BufferedImage image = ImageIO.read(imageFile);
                        images.add(image);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return images.get(rand.nextInt(images.size()));
    }

}
