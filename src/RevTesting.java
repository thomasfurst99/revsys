import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;

/**
 * Created by thomasfurst on 30/03/16.
 */
public class RevTesting implements ActionListener {

    private final int NUMBER_OF_CHOICES = 3;

    private Map<String, String> plants = new Hashtable<>();

    private JFrame frame;
    private JPanel panel;
    private JLabel image;

    private JPanel choice;

    private JLabel info;

    private JButton[] choices = new JButton[NUMBER_OF_CHOICES];

    private JButton next;

    int rightChoiceBtnIndex = -1;

    public RevTesting(String database) {
        //loadAllPlants(database);
        plants = Utils.loadPlants(database);

        frame = new JFrame("RevSys Test");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(800, 400);
        frame.setLocationRelativeTo(null);

        panel = new JPanel(new GridBagLayout());
        choice = new JPanel(new GridBagLayout());

        image = new JLabel();

        info = new JLabel();
        info.setHorizontalAlignment(JLabel.CENTER);

        for (int i = 0; i < NUMBER_OF_CHOICES; i++) {
            choices[i] = new JButton("Choice " + (i + 1));
            choices[i].addActionListener(this);
            choices[i].setOpaque(true);
        }


        next = new JButton("Další");
        next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                info.setText("");
                choices[rightChoiceBtnIndex].setBackground(null);
                generateNew();
            }
        });


        generateNew();
        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    private void rebuildUI() {
        panel.removeAll();
        choice.removeAll();

        GridBagConstraints c = new GridBagConstraints();

        c.gridy = 0;
        c.ipadx = 0;
        c.ipady = 0;
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(3, 3, 3, 3);

        for (int i = 0; i < NUMBER_OF_CHOICES; i++) {
            c.gridx = i;
            choice.add(choices[i], c);
        }

        c.gridy = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 10;
        c.weighty = 10;
        c.insets = new Insets(5, 5, 5, 5);
        panel.add(image, c);

        c.gridy++;
        c.ipady = c.ipadx = 0;
        c.weightx = c.weighty = 0;
        panel.add(choice, c);

        c.gridy++;
        panel.add(info, c);

        c.gridy++;
        panel.add(next, c);

        frame.revalidate();
        frame.repaint();
    }

    private void generateNew() {
        Random rand = new Random();
        List<String> keys = new ArrayList<>(plants.keySet());
        String key = keys.get(rand.nextInt(keys.size()));

        BufferedImage img = Utils.getRandomImgFromDir(plants.get(key), plants, key);

        double ratio = (double) Math.min((int) (frame.getHeight() * 0.7), img.getHeight()) / (double) img.getHeight();

        int width = (int) (img.getWidth() * ratio);
        int height = (int) (img.getHeight() * ratio);

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.createGraphics();
        g.drawImage(img, 0, 0, width, height, null);
        g.dispose();

        this.image = new JLabel(new ImageIcon(image));

        rightChoiceBtnIndex = rand.nextInt(NUMBER_OF_CHOICES);

        for (int i = 0; i < NUMBER_OF_CHOICES; i++) {
            if (i == rightChoiceBtnIndex) {
                choices[i].setText(key);
            } else {
                choices[i].setText(keys.get(rand.nextInt(keys.size())));
            }
        }

        next.setVisible(false);
        rebuildUI();
    }

    /**
     * Invoked when an action occurs.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == choices[rightChoiceBtnIndex]) {
            info.setText("Správně");
        } else {
            info.setText("Špatně");
        }

        choices[rightChoiceBtnIndex].setBackground(Color.GREEN);
        rebuildUI();

        next.setVisible(true);
    }
}
