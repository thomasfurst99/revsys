# RevSys #

Revision system for students. You could learn everything from plants to rocks. Just create a database, load it and learn it!

## Database creation ##

To create database simply create system of folders. In each folder you want to see in the system, there must be an 'info.txt' file. In this file you could use some Markdown features ('====' and '---' for headings and '*' for lists). Every image in folder will be visible in the app under the name of the item.

Final database should look like this:


```
#!

root
├── FirstItem
│   ├── Subitem
│   │   ├── Item1
│   │   │   ├─ info.txt
│   │   │   ├─ image1.txt
│   │   │   ├─ image2.txt
│   │   │   └─ image3.jpg
│   │   ├── Item2
│   │   │   ├─ info.txt
│   │   │   ├─ image1.txt
│   │   │   ├─ image2.txt
│   │   │   └─ image3.jpg
│   │   └── info.txt
│   └── info.txt
├── Second item
│   ├─ info.txt
│   ├─ image1.txt
│   ├─ image2.txt
│   └─ image3.jpg
└── info.txt
```

## info.txt creation ##

The info.txt file must be in every folder you want to see in app or even you just want to iterate over it. This text file should be written in Markdown (to simplify it for people without HTML knowledge, but work only '===' and '---' for headings and '*' for lists) or in HTML.

Example of Markdown info.txt:


```
#!Markdown

RevSys
-------

* Very handy utility
* You must love it
```


And the same in HTML:


```
#!HTML

<h3>RevSys<h3>

<ul>
    <li>Very handy utility</li>
    <li>You must love it</li>
</ul>
```


RevSys then convert this file into HTML and shows it when the item is clicked. First line of the files that are leaves in filesystem (no other folder within them) is used in tree and in later tests as name of the item.

## Choose from three test type ###

In this type of test you must choose the right name of the item that is shown in the picture. These items are randomly selected in leaf directories. Pictures could be in .jpg, .jpeg or .png format.

When you pick the right choice green button appears otherwise right choice is highlighted.

## Guess random ##

In this type random images are shown on the screen and you have to guess what it is. When you think you know it click on check button and check your answer.